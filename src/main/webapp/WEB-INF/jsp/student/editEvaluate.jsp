<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"  prefix="fmt" %>


<!DOCTYPE html>
<html>
<head>
    <title></title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- 引入bootstrap -->
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
    <!-- 引入JQuery  bootstrap.js-->
    <script src="/js/jquery-3.2.1.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
</head>
<body>
<!-- 顶栏 -->
<jsp:include page="top.jsp"></jsp:include>
<!-- 中间主体 -->
<div class="container" id="content">
    <div class="row">
        <jsp:include page="menu.jsp"></jsp:include>
        <div class="col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <h1 style="text-align: center;">评教课程</h1>
                    </div>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" action="/student/evaluate/add" id="editfrom" method="post">
                        <div class="form-group ">
                            <label for="inputEmail3" class="col-sm-2 control-label" >课程编号</label>
                            <div class="col-sm-10">
                                <input readonly="readonly" type="number" class="form-control" id="inputEmail3" name="courseID" placeholder="课程编号"
                                <c:if test='${evaluateMap!=null}'>
                                       value="${evaluateMap.courseID}"
                                </c:if>>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="inputEmail3" class="col-sm-2 control-label" >课程名称</label>
                            <div class="col-sm-10">
                                <input readonly="readonly" type="text" class="form-control" id="inputEmail3" name="courseName" placeholder="课程名称"
                                <c:if test='${evaluateMap!=null}'>
                                       value="${evaluateMap.courseName}"
                                </c:if>>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="inputEmail3" class="col-sm-2 control-label" >教师编号</label>
                            <div class="col-sm-10">
                                <input readonly="readonly" type="number" class="form-control" id="inputEmail3" name="teacherID" placeholder="教师编号"
                                <c:if test='${evaluateMap!=null}'>
                                       value="${evaluateMap.teacherID}"
                                </c:if>>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="inputEmail3" class="col-sm-2 control-label" >教师名称</label>
                            <div class="col-sm-10">
                                <input readonly="readonly" type="text" class="form-control" id="inputEmail3" name="teacherName" placeholder="教师名称"
                                <c:if test='${evaluateMap!=null}'>
                                       value="${evaluateMap.teacherName}"
                                </c:if>>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">评分</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" id="inputPassword3" name="score" placeholder="请输入评分">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputPassword3" class="col-sm-2 control-label">评价</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="inputPassword3" name="scoreTest" placeholder="请输入评价">
                            </div>
                        </div>
                        <div class="form-group" style="text-align: center">
                            <button class="btn btn-default" type="submit">提交</button>
                            <button class="btn btn-default" type="reset">重置</button>
                        </div>
                    </form>
                </div>

            </div>

        </div>
    </div>
</div>
<div class="container" id="footer">
    <div class="row">
        <div class="col-md-12"></div>
    </div>
</div>
</body>
<script type="text/javascript">
    $("#nav li:nth-child(3)").addClass("active")

    var collegeSelect = $("#college option");
    for (var i=0; i<collegeSelect.length; i++) {
        if (collegeSelect[i].value == '${student.collegeid}') {
            collegeSelect[i].selected = true;
        }
    }
</script>
</html>