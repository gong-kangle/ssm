<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="col-md-2">
    <ul class="nav nav-pills nav-stacked" id="nav">
        <li><a href="/teacher/showCourse">我的课程</a></li>
        <li><a href="/teacher/show">评教信息</a></li>
        <li><a href="/teacher/myShow">个人信息</a></li>
        <li><a href="/teacher/passwordRest">修改密码</a></li>
        <li><a href="/logout">退出系统</a></li>
    </ul>
</div>