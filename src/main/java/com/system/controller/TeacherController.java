package com.system.controller;

import com.system.entity.*;
import com.system.service.CourseService;
import com.system.service.EvaluateService;
import com.system.service.SelectedCourseService;
import com.system.service.TeacherService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import java.util.List;


@Controller
@RequestMapping(value = "/teacher")
public class TeacherController {

    @Resource
    private TeacherService teacherService;

    @Resource
    private CourseService courseService;

    @Resource
    private SelectedCourseService selectedCourseService;

    @Resource
    private EvaluateService service;

    // 显示我的课程
    @RequestMapping(value = "/showCourse")
    public String stuCourseShow(Model model) throws Exception {

        Subject subject = SecurityUtils.getSubject();
        String username = (String) subject.getPrincipal();

        List<CourseCustom> list = courseService.findByTeacherID(Integer.parseInt(username));
        model.addAttribute("courseList", list);

        return "teacher/showCourse";
    }

    // 显示成绩
    @RequestMapping(value = "/gradeCourse")
    public String gradeCourse(Integer id, Model model) throws Exception {
        if (id == null) {
            return "";
        }
        List<SelectedCourseCustom> list = selectedCourseService.findByCourseID(id);
        model.addAttribute("selectedCourseList", list);
        return "teacher/showGrade";
    }

    //搜索课程
    @RequestMapping(value = "/selectCourse", method = {RequestMethod.POST})
    private String selectCourse(String findByName, Model model) throws Exception {

        Subject subject = SecurityUtils.getSubject();
        String username = (String) subject.getPrincipal();

        try {
            if (findByName != null && !"".equals(findByName)){
                List<Course> list = teacherService.selectByIdAndName(Integer.parseInt(username), findByName);
                model.addAttribute("courseList", list);
            }else {
                List<CourseCustom> list = courseService.findByTeacherID(Integer.parseInt(username));
                model.addAttribute("courseList", list);
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        return "teacher/showCourse";
    }

    // 打分
    @RequestMapping(value = "/mark", method = {RequestMethod.GET})
    public String markUI(SelectedCourseCustom scc, Model model) throws Exception {

        SelectedCourseCustom selectedCourseCustom = selectedCourseService.findOne(scc);

        model.addAttribute("selectedCourse", selectedCourseCustom);

        return "teacher/mark";
    }

    @RequestMapping(value = "/show", method = {RequestMethod.GET})
    public String show(Model model, Integer page){
        try {
            List<Evaluate> list = null;
            //页码对象
            PagingVO pagingVO = new PagingVO();
            //设置总页数
            pagingVO.setTotalCount(service.getCount());
            if (page == null || page == 0) {
                pagingVO.setToPageNo(1);
                list = service.selectByAll(1);
            } else {
                pagingVO.setToPageNo(page);
                list = service.selectByAll(page);
            }
            model.addAttribute("evaluateList", list);
            model.addAttribute("pagingVO", pagingVO);
        }catch (Exception e) {
            e.printStackTrace();
            System.out.println("错误信息：" + e.getMessage());
        }
        return "teacher/show";
    }

    // 打分
    @RequestMapping(value = "/mark", method = {RequestMethod.POST})
    public String mark(SelectedCourseCustom scc) throws Exception {

        selectedCourseService.updataOne(scc);

        return "redirect:/teacher/gradeCourse?id="+scc.getCourseid();
    }

    //修改密码
    @RequestMapping(value = "/passwordRest")
    public String passwordRest() throws Exception {
        return "teacher/passwordRest";
    }

    @RequestMapping(value = "/updateOne")
    public String updateOne(Integer courseID) {
        System.out.println("courseID：" + courseID);
        courseService.updateOne(courseID);
        return "redirect:/teacher/showCourse";
    }

    @RequestMapping(value = "/updateTwo")
    public String updateTwo(Integer courseID) {
        System.out.println("courseID：" + courseID);
        courseService.updateTwo(courseID);
        return "redirect:/teacher/showCourse";
    }

    @RequestMapping(value = "/myShow")
    public String myShow(Model model) {
        Subject subject = SecurityUtils.getSubject();
        String username = (String) subject.getPrincipal();

        Teacher teacher = teacherService.selectAll(Integer.parseInt(username));
        model.addAttribute("teacherMess", teacher);

        return "teacher/myShow";
    }

}
