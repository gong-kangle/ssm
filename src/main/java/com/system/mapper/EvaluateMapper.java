package com.system.mapper;

import com.system.entity.Evaluate;
import com.system.entity.PagingVO;

import java.util.List;

public interface EvaluateMapper {

    /**
     * 查询所有评教信息
     * @param pagingVO
     * @return
     */
    List<Evaluate> selectByAll(PagingVO pagingVO);

    /**
     * 添加评教信息
     * @param evaluate
     */
    void add(Evaluate evaluate);

    /**
     * 查询有多少条评教记录
     * @return
     */
    int getCount();

}
