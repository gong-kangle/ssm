package com.system.mapper;

import com.system.entity.CourseCustom;
import com.system.entity.PagingVO;

import java.util.List;

public interface CourseMapperCustom {

    //分页查询学生信息
    List<CourseCustom> findByPaging(PagingVO pagingVO) throws Exception;

}
