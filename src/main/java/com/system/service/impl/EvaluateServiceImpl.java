package com.system.service.impl;

import com.system.entity.Evaluate;
import com.system.entity.PagingVO;
import com.system.mapper.EvaluateMapper;
import com.system.service.EvaluateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EvaluateServiceImpl implements EvaluateService {

    @Autowired
    private EvaluateMapper mapper;

    /**
     * 查询所有评教信息
     *
     * @param toPageNo
     * @return
     */
    public List<Evaluate> selectByAll(Integer toPageNo) {
        PagingVO pagingVO = new PagingVO();
        pagingVO.setToPageNo(toPageNo);
        List<Evaluate> list = mapper.selectByAll(pagingVO);
        return list;
    }

    /**
     * 添加评教信息
     *
     * @param evaluate
     */
    public void add(Evaluate evaluate) {
        mapper.add(evaluate);
    }

    /**
     * 查询有多少条评教记录
     *
     * @return
     */
    public int getCount() {
        return mapper.getCount();
    }
}
