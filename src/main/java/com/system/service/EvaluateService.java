package com.system.service;

import com.system.entity.Evaluate;
import com.system.entity.PagingVO;

import java.util.List;

public interface EvaluateService {

    /**
     * 查询所有评教信息
     * @param toPageNo
     * @return
     */
    List<Evaluate> selectByAll(Integer toPageNo);

    /**
     * 添加评教信息
     * @param evaluate
     */
    void add(Evaluate evaluate);

    /**
     * 查询有多少条评教记录
     * @return
     */
    int getCount();
}
