package com.system.entity;

public class Evaluate {

    private Integer id;
    private Integer courseID;
    private Integer teacherID;
    private Integer studentID;
    private Integer score;
    private String scoreTest;
    private String teacherName;
    private String studentName;
    private String courseName;

    public Evaluate() {
    }

    public Evaluate(Integer id, Integer courseID, Integer teacherID, Integer studentID, Integer score, String scoreTest, String teacherName, String studentName, String courseName) {
        this.id = id;
        this.courseID = courseID;
        this.teacherID = teacherID;
        this.studentID = studentID;
        this.score = score;
        this.scoreTest = scoreTest;
        this.teacherName = teacherName;
        this.studentName = studentName;
        this.courseName = courseName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCourseID() {
        return courseID;
    }

    public void setCourseID(Integer courseID) {
        this.courseID = courseID;
    }

    public Integer getTeacherID() {
        return teacherID;
    }

    public void setTeacherID(Integer teacherID) {
        this.teacherID = teacherID;
    }

    public Integer getStudentID() {
        return studentID;
    }

    public void setStudentID(Integer studentID) {
        this.studentID = studentID;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public String getScoreTest() {
        return scoreTest;
    }

    public void setScoreTest(String scoreTest) {
        this.scoreTest = scoreTest;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }
}
